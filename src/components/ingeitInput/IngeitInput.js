import React from "react";
import PropTypes from 'prop-types';
import TextField from "@material-ui/core/TextField";

const IngeitInput = ({
  name,
  label,
  value,
  handleChange,
  setFieldTouched,
  touched,
  errors,
  type
}) => {
  const change = (name, e) => {
    handleChange(e);
    setFieldTouched(name, true, false);
  };

  return (
    <TextField
      id={name}
      name={name}
      label={label}
      value={value}
      fullWidth
      type={type}
      onChange={e => change(name, e)}
      margin="normal"
      helperText={touched[name] ? errors[name] : ""}
      error={touched[name] && Boolean(errors[name])}
    />
  );
};

IngeitInput.defaultProps = {
  name: '',
  label: '',
  value: '',
  type: '',
  touched: {},
  errors: {},
  handleChange: () => null,
  setFieldTouched: () => null
};

IngeitInput.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.string,
  handleChange: PropTypes.func,
  setFieldTouched: PropTypes.func,
  touched: PropTypes.object,
  errors: PropTypes.object,
  type: PropTypes.string
};

export default IngeitInput;

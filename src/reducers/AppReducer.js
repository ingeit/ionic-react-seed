export const UPDATE_STATE = "UPDATE_STATE";

export const initialAppState = {
  id: null,
};

const updateAppState = ({stepIndex, config}, state) => {
  return { ...state, [stepIndex]: config };
};

export const AppReducer = (state, action) => {
  switch (action.type) {
    case UPDATE_STATE:
      return updateAppState(action.configToUpdate , state);

    default:
      return state;
  }
};

import axios from 'axios';

const http = axios.create();

http.defaults.baseURL = `https://jsonplaceholder.typicode.com/`;
// http.defaults.baseURL = `${window.location.protocol}//${window.location.host}/api/`;

http.interceptors.request.use(config => {
  config.headers = {
    Authorization: localStorage.getItem('token'),
    'Content-Type': 'application/json'
  };
  return config;
});

http.interceptors.response.use(resp => {
  if (resp.data.errMsg && resp.data.status === 401)
    return Promise.reject(resp);

  if(resp.status === 200)
    return resp.data;

  return resp;
});

export default http;

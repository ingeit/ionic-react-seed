import React from "react";
import {
  IonApp,
  IonTabs,
  IonTabBar,
  IonTabButton,
  IonLabel,
  IonRouterOutlet,
  IonIcon
} from "@ionic/react";
import { Route, Redirect } from "react-router-dom";
import { IonPage } from "@ionic/react";
import Tab1 from "../tab1/Tab1";
import Tab2 from "../tab2/Tab2";
import Tab3 from "../tab3/Tab3";
import Tab4 from "../tab4/Tab4";
import NotFound from "../notFound/NotFound";

const Tabs = () => (
  <IonApp>
    <IonPage>
      <Route exact path="/" render={() => <Redirect to="/tab1" />} />
      <IonTabs>
        <IonRouterOutlet>
          <Route path="/:tab(tab1)" component={Tab1} exact />
          <Route path="/:tab(tab2)" component={Tab2} exact />
          <Route path="/:tab(tab3)" component={Tab3} exact />
          <Route path="/:tab(tab1)/tab4" component={Tab4} />
          <Route path="*" component={NotFound} />
        </IonRouterOutlet>

        <IonTabBar style={{ display: 'flex' }} slot="bottom">
          <IonTabButton tab="home" href="/tab1">
            <IonIcon name="home" />
            <IonLabel>Tab 1</IonLabel>
          </IonTabButton>
          <IonTabButton tab="settings" href="/tab2">
            <IonIcon name="settings" />
            <IonLabel>Tab 2</IonLabel>
          </IonTabButton>
          <IonTabButton tab="about" href="/tab3">
            <IonIcon name="information-circle" />
            <IonLabel>Tab 3</IonLabel>
          </IonTabButton>
        </IonTabBar>
      </IonTabs>
    </IonPage>
  </IonApp>
);

export default Tabs;

import jwtDecode from 'jwt-decode';

const isJwtTokenStored = () => {
  return Boolean(localStorage.token && localStorage.token !== '');
}

const getValueFromJwtPayload = key => {
  if (isJwtTokenStored()) {
    const decodedToken = jwtDecode(localStorage.token);
    if (!key) {
      return null;
    }
    return decodedToken[key];
  }
  return null;
}

export default getValueFromJwtPayload;
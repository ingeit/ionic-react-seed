import React from "react";
import "@ionic/core/css/core.css";
import "@ionic/core/css/ionic.bundle.css";
import { IonApp } from "@ionic/react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Tabs from "./pages/tabs";
import Login from "./pages/login/Login";
import PrivateRoute from "./components/auth/PrivateRoute";
import AppContext from "./contexts/AppContexts";
import "./App.scss";

const App = () => {

  const providerValue = {
    companyName: 'Ingeit S.A'
  };

  return (
    <AppContext.Provider value={ providerValue }>
      <IonApp>
        <Router>
          <Switch>
            <Route exact path="/login" component={Login} />
            <PrivateRoute path="/" component={Tabs} />
          </Switch>
        </Router>
      </IonApp>
    </AppContext.Provider>
  );
};

export default App;

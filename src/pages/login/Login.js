import React from "react";
import { IonContent, IonApp, IonPage } from "@ionic/react";
import * as Yup from "yup";
import { Formik } from "formik";
import Form from './Form';

const validationSchema = Yup.object({
  name: Yup.string("Enter a name").required("Name is required"),
  email: Yup.string("Enter your email")
    .email("Enter a valid email")
    .required("Email is required"),
  password: Yup.string("")
    .min(8, "Password must contain at least 8 characters")
    .required("Enter your password"),
  confirmPassword: Yup.string("Enter your password")
    .required("Confirm your password")
    .oneOf([Yup.ref("password")], "Password does not match")
});

const Login = () => {
  const values = { name: "", email: "", confirmPassword: "", password: "" };

  return (
    <IonApp>
      <IonPage>
        <IonContent>
          <h1>Form</h1>
          <Formik
            render={props => <Form {...props} />}
            initialValues={values}
            validationSchema={validationSchema}
            onSubmit={(values, actions) => {
              console.log(values)
              actions.setSubmitting(false);
            }}
          />
        </IonContent>
      </IonPage>
    </IonApp>
  );
};

export default Login;

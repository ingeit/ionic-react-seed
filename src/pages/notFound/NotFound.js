import React from "react";
import { IonContent } from "@ionic/react";

const NotFound = () => (
  <IonContent>
    <p> No se encontro el recurso solicitado</p>
  </IonContent>
);

export default NotFound;

import React from "react";
import { IonButton } from "@ionic/react";
import TextField from "@material-ui/core/TextField";
import IngeitInput from "../../components/ingeitInput/IngeitInput";

const Form = props => {
  const {
    values: { name, email, password, confirmPassword },
    errors,
    touched,
    handleChange,
    handleSubmit,
    isValid,
    setFieldTouched
  } = props;

  const change = (name, e) => {
    handleChange(e);
    setFieldTouched(name, true, false);
  };

  return (
    <form onSubmit={handleSubmit}>
      <IngeitInput name="name" label="Nombre" value={name} {...props} />
      <TextField
        id="email"
        name="email"
        label="Email"
        value={email}
        fullWidth
        onChange={e => change("email", e)}
        margin="normal"
        helperText={touched.email ? errors.email : ""}
        error={touched.email && Boolean(errors.email)}
      />
      <TextField
        id="password"
        name="password"
        label="password"
        value={password}
        fullWidth
        onChange={e => change("password", e)}
        margin="normal"
        helperText={touched.password ? errors.password : ""}
        error={touched.password && Boolean(errors.password)}
      />
      <TextField
        id="confirmPassword"
        name="confirmPassword"
        label="confirmPassword"
        value={confirmPassword}
        fullWidth
        onChange={e => change("confirmPassword", e)}
        margin="normal"
        helperText={touched.confirmPassword ? errors.confirmPassword : ""}
        error={touched.confirmPassword && Boolean(errors.confirmPassword)}
      />
      <IonButton expand="block" type="submit" disabled={!isValid}>
        <input style={{ display: "none" }} type="submit" />
        Submit
      </IonButton>
    </form>
  );
};

export default Form;

import React, { useEffect } from "react";
import {
  IonContent,
  IonHeader,
  IonToolbar,
  IonButtons,
  IonBackButton,
  IonTitle,
  IonIcon,
  IonList,
  IonSelectOption,
  IonSelect,
  IonItem,
  IonLabel,
  IonDatetime
} from "@ionic/react";
import { Link } from "react-router-dom";
import http from "../../herlpers/apiHelper";

const Tab4 = ({match}) => {

  useEffect(() => {
    http.get('todos').then( res => console.log(res)).catch( err => console.log(err));
  },[]);

  return (
    <>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton
              goBack={ () => {} }
              defaultHref={`/${match.params.tab}`}
            />
          </IonButtons>
          <IonTitle>Tab 4</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <div className="ion-padding about-info">
          <h4>Just a regular title Tab 2</h4>
          <Link to="/tab1">Show sidebar</Link>
          <IonList lines="none">
            <IonItem>
              <IonIcon name="calendar" slot="start" />
              <IonLabel>Date Picker</IonLabel>
              <IonDatetime
                displayFormat="MMM DD, YYYY"
                max="2056"
                value={null}
              />
            </IonItem>

            <IonItem>
              <IonIcon name="pin" slot="start" />
              <IonLabel>Selector</IonLabel>
              <IonSelect>
                <IonSelectOption value="madison" selected>
                  Madison, WI
                </IonSelectOption>
                <IonSelectOption value="austin">Austin, TX</IonSelectOption>
                <IonSelectOption value="chicago">Chicago, IL</IonSelectOption>
                <IonSelectOption value="seattle">Seattle, WA</IonSelectOption>
              </IonSelect>
            </IonItem>
          </IonList>

          <p>Just a regular paragrah passing by</p>
        </div>
      </IonContent>
    </>
  );
};

export default Tab4;

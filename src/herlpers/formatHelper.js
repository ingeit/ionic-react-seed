import moment from 'moment';
moment.locale('es');

//currency
const currencyFormat = value => `$ ${Number(value).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}`;

//date
const formatDateUTC = dateValue => dateValue ? moment.utc(dateValue).format('DD/MM/YY') : moment.utc().format();

const formatDate = dateValue => moment(dateValue).format('MMM YYYY');

const getLocalTimestamp = dateValue => moment.utc(dateValue).local().format('MM/DD/YY h:mm A');

const getRequestIdTimestamp = () => moment.utc().format('YYMMDD-HH:mm:ss.SSS');

//string
const titleCase = value => value.toLocaleLowerCase().split(" ")
  .map(str => str.charAt(0).toUpperCase() + str.slice(1))
  .join(" ");

const getInitialsName = value => {
  let splitUserName = value.split(" ");
  const result = (splitUserName.length > 1)
    ?(splitUserName[0].substring(0, 1) + splitUserName[1].substring(0, 1)).toUpperCase()
    :value.substring(0, 2).toUpperCase();
  return result;
};

export {
  formatDateUTC,
  formatDate,
  getLocalTimestamp,
  currencyFormat,
  getRequestIdTimestamp,
  getInitialsName,
  titleCase
};